// soal 1 - start here

// buatlah variabel seperti di bawah ini
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
// urutkan array di atas dan tampilkan data seperti output di bawah ini (dengan menggunakan loop):

// 1. Tokek
// 2. Komodo
// 3. Cicak
// 4. Ular
// 5. Buaya

// soal 1 - end here
console.log("\nJawaban no.1");
//jawaban soal 1 - start here

daftarHewan.sort()

daftarHewan.forEach(function (item) {
   console.log(item + "\n");
})

//jawaban soal 1 - end here



//jawaban soal 2 - start here

function introduce(data) {
   return "Nama saya " + data['name'] + ", umur saya " + data['age'] + " tahun, alamat saya di " + data['address'] + ", dan saya punya hobby yaitu " + data['hobby'];
}

//jawaban soal 2 - end here
console.log("\nJawaban no.2");
// soal 2 - start here

// Tulislah sebuah function dengan nama introduce() yang memproses paramater yang dikirim menjadi sebuah kalimat perkenalan seperti berikut: "Nama saya [name], umur saya [age] tahun, alamat saya di [address], dan saya punya hobby yaitu [hobby]!"

var data = { name: "John", age: 30, address: "Jalan Pelesiran", hobby: "Gaming" }

var perkenalan = introduce(data)
console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming" 

// soal 2 - end here

//jawaban soal 3 - start here

function hitung_huruf_vokal(nama) {
   var str = nama.match(/[aeiou]/gi);
   return str === null ? 0 : str.length;
}

//jawaban soal 3 - end here
console.log("\nJawaban no.3");
// soal 3 - start here

// Tulislah sebuah function dengan nama hitung_huruf_vokal() yang menerima parameter sebuah string, kemudian memproses tersebut sehingga menghasilkan total jumlah huruf vokal dalam string tersebut.

var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1, hitung_2) // 3 2

// soal 3 - start here


//jawaban soal 4 - start here

function hitung(int) {
   return (int * 2) - 2;
}

//jawaban soal 4 - end here
console.log("\nJawaban no.4");
// soal 4 - start here

// Buatlah sebuah function dengan nama hitung() yang menerima parameter sebuah integer dan mengembalikan nilai sebuah integer, dengan contoh input dan otput sebagai berikut.

console.log(hitung(0)) // -2
console.log(hitung(1)) // 0
console.log(hitung(2)) // 2
console.log(hitung(3)) // 4
console.log(hitung(5)) // 8

// soal 4 - end here